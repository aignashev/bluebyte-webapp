package config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.bluebyte.core.entity.User;
import com.jolbox.bonecp.BoneCPDataSource;


/**
 * 
 * @author ignashev
 *
 */

@Configuration
@EnableTransactionManagement
@PropertySource(
    {
        "classpath:jdbc.properties",
        "classpath:hibernate.properties"
    })
@ComponentScan(value = "com.bluebyte")
@Import({ SecurityConfig.class })
public class ApplicationContext
{

    public static final String hibernate_connection_autocommit = "hibernate.connection.autocommit";

    public static final String hibernate_format_sql = "hibernate.format_sql";

    public static final String hibernate_hbm2ddl_auto = "hibernate.hbm2ddl.auto";

    public static final String hibernate_show_sql = "hibernate.show_sql";

    public static final String hibernate_current_session_context_class = "hibernate.current_session_context_class";

    public static final String hibernate_dialect = "hibernate.dialect";

    public static final String package_to_scan = "com.bluebyte.core.entity";

    @Autowired
    private Environment env;

    @Bean
    public DataSource dataSource() {
        final JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
        dsLookup.setResourceRef(true);
        DataSource dataSource = dsLookup.getDataSource("jdbc/bluebytewaDB");
        return dataSource;
    } 
    
//    @Bean(destroyMethod = "close")
//    public DataSource dataSource()
//    {
//        BoneCPDataSource dataSource = new BoneCPDataSource();
//        dataSource.setDriverClass(env.getProperty("jdbc.driverClassName"));
//        dataSource.setJdbcUrl(env.getProperty("jdbc.url"));
//        dataSource.setUsername(env.getProperty("jdbc.username"));
//        dataSource.setPassword(env.getProperty("jdbc.password"));
//        dataSource.setIdleConnectionTestPeriodInMinutes(60);
//        dataSource.setIdleMaxAgeInMinutes(240);
//        dataSource.setMaxConnectionsPerPartition(10);
//        dataSource.setMinConnectionsPerPartition(1);
//        dataSource.setPartitionCount(1);
//        dataSource.setAcquireIncrement(5);
//        dataSource.setStatementsCacheSize(100);
//        return dataSource;
//    }
    @Bean
    @Autowired
    public LocalSessionFactoryBean sessionFactory(DataSource datasouce)
    {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(datasouce);
        sessionFactory.setPackagesToScan(package_to_scan);
        Properties hibernateProperties = new Properties();
        hibernateProperties.put(hibernate_dialect, env.getProperty(hibernate_dialect));
        hibernateProperties
            .put(hibernate_current_session_context_class, env.getProperty(hibernate_current_session_context_class));
        hibernateProperties.put(hibernate_connection_autocommit, env.getProperty(hibernate_connection_autocommit));
        hibernateProperties.put(hibernate_format_sql, env.getProperty(hibernate_format_sql));
        hibernateProperties.put(hibernate_hbm2ddl_auto, env.getProperty(hibernate_hbm2ddl_auto));
        hibernateProperties.put(hibernate_show_sql, env.getProperty(hibernate_show_sql));
        sessionFactory.setHibernateProperties(hibernateProperties);
        return sessionFactory;
    }
    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory)
    {
        HibernateTransactionManager txManager = new HibernateTransactionManager(sessionFactory);
        return txManager;
    }
    
    @Bean
    public Jaxb2Marshaller marshler() {
    	 Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
         jaxb2Marshaller.setClassesToBeBound(User.class);
         Resource xsdSchema = new ClassPathResource("UserItems.xsd");
         
         jaxb2Marshaller.setSchema(xsdSchema);
         return jaxb2Marshaller;
    }
}
