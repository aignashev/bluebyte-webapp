package config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.bluebyte.core.service.impl.CustomAuthenticationProvider;
import com.bluebyte.core.service.impl.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	private CustomAuthenticationProvider customAuthenticationProvider;

	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.authenticationProvider(customAuthenticationProvider);
//        auth
//        .inMemoryAuthentication()
//        .withUser("user").password("password").roles("Admin");
//        auth
//        .inMemoryAuthentication()
//        .withUser("user1").password("password1").roles("Guest");
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    
    http.authorizeRequests()
    .antMatchers("/items/**").access("hasAnyRole('Admin', 'Guest')")
    .and().formLogin().and().
    logout().logoutSuccessUrl("/items").deleteCookies("JSESSIONID").and();
    
    }
    
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    	auth.userDetailsService(customUserDetailsService);
    }

}
