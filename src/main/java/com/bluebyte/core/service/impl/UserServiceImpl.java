package com.bluebyte.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluebyte.core.dao.UserDao;
import com.bluebyte.core.entity.User;
import com.bluebyte.core.service.UserService;

/**
 * 
 * @author ignashev
 *
 */

@Service
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao userDao;
	
	@Override
	public User getUserByUsername(String username) {
		return userDao.getUserByUsername(username);
	}

}
