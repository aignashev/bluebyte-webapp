package com.bluebyte.core.service.impl;

import java.net.URI;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.springframework.stereotype.Service;

import com.bluebyte.core.entity.service.Item;
import com.bluebyte.core.entity.service.User;
import com.bluebyte.core.entity.service.UsersListResponse;
import com.bluebyte.core.service.ItemsService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

@Service
public class ItemsServiceImpl implements ItemsService {

	@Override
	public List<Item> getItemsByUsername(String username) {
		ClientConfig config = new DefaultClientConfig();

		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		User user =   service.path("user").path(username).accept(MediaType.TEXT_XML).get(User.class);
		return user.getItems();
	}

	private static URI getBaseURI() {
		
//		return UriBuilder.fromUri(
//				"http://bluebytews-ignashev.rhcloud.com/services/")
//				.build();
		
		return UriBuilder.fromUri(
				"http://localhost:8080/bluebyte-service/services/")
				.build();
	}

	public List<String> getAllUsernames() {
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		UsersListResponse response =   service.path("user").path("list").accept(MediaType.TEXT_XML).get(UsersListResponse.class);
		return response.getUsername();
	}

}
