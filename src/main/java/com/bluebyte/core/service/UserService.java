package com.bluebyte.core.service;

import com.bluebyte.core.entity.User;


/**
 * 
 * @author ignashev
 *
 */

public interface UserService {

	public User getUserByUsername(String username);
	
}
