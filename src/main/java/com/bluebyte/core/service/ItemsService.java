package com.bluebyte.core.service;

import java.util.List;

import com.bluebyte.core.entity.service.Item;

public interface ItemsService {
	public List<Item> getItemsByUsername(String username);
	public List<String> getAllUsernames();
	
}
