package com.bluebyte.core.dao.impl;

import java.io.Serializable;

import ml.rugal.sshcommon.hibernate.HibernateBaseDao;

import org.hibernate.Query;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.bluebyte.core.dao.UserDao;
import com.bluebyte.core.entity.User;

/**
 * 
 * @author ignashev
 *
 */

@Repository
public class UserDaoImpl extends HibernateBaseDao<User, Serializable> implements UserDao {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(UserDaoImpl.class
            .getName());
    
	@Override
	public User getUserByUsername(String username) {
		Query q = getSession().createQuery("from User u join fetch u.role ur where u.username=:username");
		q.setParameter("username", username);
		return (User) q.uniqueResult();
	}

	@Override
	protected Class<User> getEntityClass() {
		return User.class;
	}

}
