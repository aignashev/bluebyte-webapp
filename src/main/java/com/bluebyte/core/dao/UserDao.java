package com.bluebyte.core.dao;

import com.bluebyte.core.entity.User;

/**
 * 
 * @author ignashev
 *
 */

public interface UserDao {

	public User getUserByUsername(String username);
}
