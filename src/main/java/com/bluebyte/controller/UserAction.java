package com.bluebyte.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bluebyte.core.entity.User;
import com.bluebyte.core.service.UserService;

/**
 * 
 * @author ignashev
 *
 */


@Controller
@RequestMapping(value = "/services/user")
public class UserAction {
	
    	private static final Logger LOG = LoggerFactory.getLogger(UserAction.class.getName());

	
		@Autowired
		private UserService userService;
	
	 	@ResponseBody
	    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
	    public User getUser(@PathVariable("username") String username)
	    {
	 		User user = userService.getUserByUsername(username);
	        return user;
	    }
}
