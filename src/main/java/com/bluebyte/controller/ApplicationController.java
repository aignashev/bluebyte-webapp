package com.bluebyte.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.bluebyte.core.entity.service.Item;
import com.bluebyte.core.service.ItemsService;

@Controller
public class ApplicationController {
	
	@Autowired
	private ItemsService itemsService;

	@RequestMapping(value = { "/", "/helloworld**" }, method = RequestMethod.GET)
	public ModelAndView welcomePage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title",
				"Spring Security 3.2.3 Hello World Application");
		model.addObject("message", "Welcome Page !");
		model.setViewName("helloworld");
		return model;

	}

	@RequestMapping(value = "/items/**", method = RequestMethod.GET)
	public ModelAndView protectedPage(@RequestParam(value = "selectUsername", required=false) String selectUsername ) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	      String username = auth.getName(); 
	      ModelAndView model = new ModelAndView();
	      String usernameForSearch = auth.getName();
	      Collection<? extends GrantedAuthority> roles =  auth.getAuthorities();
	      if (isUserAdmin(roles)) {
	    	  List<String> users = itemsService.getAllUsernames();
	    	  if (selectUsername != null) {
	    		  List<Item> items =  itemsService.getItemsByUsername(selectUsername);
	    		  model.addObject("items", items);
	    		  model.addObject("currentSelectedUsername", selectUsername);
	    	  }
	    	  model.addObject("role", "Admin");
	    	  model.addObject("allUsers", users);
	    	  
	      } else if (isUserGuest(roles)) {
	  		List<Item> items =  itemsService.getItemsByUsername(usernameForSearch);
			model.addObject("items", items);
	    	  model.addObject("role", "Guest");
	      }
	      
		
		model.addObject("title", "Spring Security 3.2.3 Hello World");
		model.addObject("username", username
				);
		
		return model;
	}

	private boolean isUserGuest(Collection<? extends GrantedAuthority> roles) {
		for (GrantedAuthority grantedAuthority : roles) {
			if ("Guest".equals(grantedAuthority.getAuthority())) {
				return true;
			}
		}
		return false;
	}

	private boolean isUserAdmin(Collection<? extends GrantedAuthority> roles) {
		for (GrantedAuthority grantedAuthority : roles) {
			if ("Admin".equals(grantedAuthority.getAuthority())) {
				return true;
			}
		}
		return false;
	}
}
