<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Protected page</title>
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
<link rel="stylesheet" href="<c:url value="/css/custom.css" />">

<!-- Optional theme -->
<link rel="stylesheet" href="<c:url value="/css/bootstrap-theme.min.css" />">

<!-- Latest compiled and minified JavaScript -->
<script src="<c:url value="/js/jquery-1.11.0.min.js" />"></script>
<script src="<c:url value="/js/bootstrap.min.js" />"></script>
</head>
<body>
 <div class="container">
	  <div class="header">
        <nav>
        	<div class="pull-right">
        		<ul class="nav nav-pills pull-right">
            
          </ul>
        	</div>
          
        </nav>
        <h3 class="text-muted">Items list</h3>
      </div>
      
      <div class="row marketing">
        <div class="col-lg-6">
          <h4><a href="<c:url value="/items"  />">Items for user</a></h4>

          
        </div>

      </div>

      <footer class="footer">
        <p>&copy; 2015</p>
      </footer>
 </div>

</body>
</html>