<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" 
           uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Items page</title>
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
<link rel="stylesheet" href="<c:url value="/css/custom.css" />">

<!-- Optional theme -->
<link rel="stylesheet" href="<c:url value="/css/bootstrap-theme.min.css" />">

<!-- Latest compiled and minified JavaScript -->
<script src="<c:url value="/js/jquery-1.11.0.min.js" />"></script>
<script src="<c:url value="/js/bootstrap.min.js" />"></script>

</head>
<body>
 <div class="container">
	  <div class="header">
        <nav>
        	<div class="pull-right">
        		
        		<ul class="nav nav-pills pull-right">
            
             <c:url var="logoutUrl" value="logout" />
    <form class="form-inline" action="${logoutUrl}" method="post">
      <input type="submit" value="Log out" class="btn" />
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
            
        	</div>
          
        </nav>
        <h3 class="text-muted"><span>User <span class="label label-primary">${username}</span> (Logged as <span class="label label-info">${role}</span>)</span></h3>
      </div>
      
<sec:authorize access="hasRole('Guest')">
      <div class="row marketing">
        <div class="col-lg-6">
          <h4>Items for user <span class="label label-primary">${username}</span></h4>

          <table class="table table-striped">
          	<thead>
          		<th>Name</th>
          		<th>Game</th>
          		<th>Quantity</th>
          		<th>Expiration Date</th>
          	</thead>
 <c:forEach items="${items}" var="item">
 	<tr>
 		<td>${item.name}</td>
 		<td>${item.game}</td>
 		<td>${item.quantity}</td>
 		<td> <fmt:formatDate pattern="dd-MM-yyyy" 
            value="${item.expirationDate}"/> </td>
 	</tr>
 </c:forEach>
			</table>
        </div>

      </div>
</sec:authorize>
<sec:authorize access="hasRole('Admin')">
   <div class="row marketing">
        <div class="col-lg-6">
	<div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
   <c:if test="${currentSelectedUsername != null}">
    ${currentSelectedUsername}
   </c:if>
   <c:if test="${currentSelectedUsername == null}">
    Chose User for request
   </c:if>
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
  	<c:forEach items="${allUsers}" var="username">
    	<li role="presentation"><a role="menuitem" tabindex="-1" href="<c:url value="?selectUsername=${username}" />">${username}</a></li>
    </c:forEach>
   
  </ul>
  
</div>
          <h4>Items for user <span class="label label-primary">${currentSelectedUsername}</span></h4>

          <table class="table table-striped">
          	<thead>
          		<th>Name</th>
          		<th>Game</th>
          		<th>Quantity</th>
          		<th>Expiration Date</th>
          	</thead>
 <c:forEach items="${items}" var="item">
 	<tr>
 		<td>${item.name}</td>
 		<td>${item.game}</td>
 		<td>${item.quantity}</td>
 		<td><fmt:formatDate pattern="dd-MM-yyyy" 
            value="${item.expirationDate}"/></td>
 	</tr>
 </c:forEach>
			</table>

</div>
</div>
</sec:authorize>

      <footer class="footer">
        <p>&copy; 2015</p>
      </footer>
 </div>

</body>
</html>